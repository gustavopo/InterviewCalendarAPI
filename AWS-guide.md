#######################################################################
#
# This file will be used to register the AWS  commands done in the udemy course for setting up pipelines
# First we will deploy manually an app via AWS Elastic Beanstalk
# 
# The next step will automate the last one.
# We will put our application on AWS S3 and S3 will connect with AWS Elastic to deploy the most recent version on the application
#
#######################################################################


### Getting started with S3

1 - Create a bucket (a bucket is like a "folder")
2 - Use a GitLab group to organize project. This is good to store shared variables, for example
